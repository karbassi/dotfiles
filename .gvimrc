set go-=T " Hide toolbar on launch
set guifont=Menlo:h13 " Set default font

" PeepOpen (,o)
map <unique> <silent> <Leader>o <Plug>PeepOpen
